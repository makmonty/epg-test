# Epg

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4, and it should be installed to run the project. You can do so with `npm install -g @angular/cli`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

You can use the `-prod` either with the `serve` or the `build` command to generate a production build.

A production build generates minified files: uglified files, mangled names and tree shaking (no unused modules).

## Test

Run `ng test` to run unit tests. Make sure you defined the environment variable `CHROME_BIN`. Do it by running `export CHROME_BIN=/path/to/chrome/or/chromium`

Run `ng e2e` to run E2E tests.

## External dependencies

The only external dependency, apart from the Angular core dependencies, is [Bootstrap](http://getbootstrap.com), to not to spend too much time styling and focus on the logic.

## Decisions made

* I decided to build the workflow as a tree. It would make the requirement of no orphan boxes easier to accomplish, and also would make the addition and removal of the boxes more intuitive (and also easier to develop). It has the downside of no loops allowed, and will force to have more than one End box. **This is a defect**, as the requirements say specifically that there should be only one End box. As a possible fix, I would make the End box a virtual box, so that all leaves of the tree would lead to it.
* I built a service to manage the storage of the workflows using `localStorage`. I could have done it using Observers, as it would be in a real use case with asynchronous calls to a backend, but again, no specific requirements about it so I just went ahead with my synchronous database model.
* The Login form doesn't work as there is no real data to work with. It will just make sure you fill it with valid strings, and the Login button leads to the Workflows section.
* I did unit and e2e tests for the login page, and unit tests for the WorkflowsService.
