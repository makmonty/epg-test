import { browser, promise } from 'protractor';
import { LoginPage } from './login.po';

describe('epg App', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
  });

  it('should display the email input', () => {
    page.navigateTo();
    expect(page.getEmailInput().isPresent()).toBeTruthy();
  });
  it('should display the password input', () => {
    page.navigateTo();
    expect(page.getPasswordInput().isPresent()).toBeTruthy();
  });
  it('should display the submit button', () => {
    page.navigateTo();
    expect(page.getSubmitButton().isPresent()).toBeTruthy();
  });
  it('should display email errors when trying to submit with no email', (next) => {
    page.navigateTo();
    page.submit()
      .then(() => {
        expect(page.getEmailFieldErrors().isPresent()).toBeTruthy();
        next();
      });
  });
  it('should display email error when trying to submit a wrong email', (next) => {
    page.navigateTo();
    page.fillEmail('wrongemail')
      .then(() => {
        page.submit()
          .then(() => {
            expect(page.getEmailFieldErrors().isPresent());
            next();
          });
      });
  });
  it('should not display email errors when trying to submit correct email', (next) => {
    page.navigateTo();
    page.fillEmail('email@test.com')
      .then(() => {
        page.submit()
          .then(() => {
            expect(page.getEmailFieldErrors().isPresent()).toBeFalsy();
            next();
          });
      });
  });
  it('should display password error when trying to submit with no email', (next) => {
    page.navigateTo();
    page.submit()
      .then(() => {
        expect(page.getPasswordInput().isPresent()).toBeTruthy();
        next();
      });
  });
  it('should navigate to /workflows when inputs are correct', (next) => {
    page.navigateTo();
    let promises = [
      page.fillEmail('email@test.com'),
      page.fillPassword('password')
    ];

    promise.all(promises)
      .then(() => {
        page.submit()
          .then(() => {
            browser.getCurrentUrl()
              .then((url) => {
                expect(url).toContain('/workflows');
                next();
              });
          });
      });
  });
});
