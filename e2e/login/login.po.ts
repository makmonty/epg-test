import { browser, by, element, promise, ElementFinder } from 'protractor';

export class LoginPage {
  navigateTo() {
    return browser.get('/');
  }

  getEmailInput(): ElementFinder {
    return element(by.name('email'));
  }

  getPasswordInput(): ElementFinder {
    return element(by.name('password'));
  }

  getSubmitButton(): ElementFinder {
    return element(by.css('[type="submit"]'));
  }

  getEmailFieldErrors(): ElementFinder {
    return this.getEmailInput()
      .element(by.xpath('..'))
      .element(by.css('.alert'));
  }

  getPasswordFieldErrors(): ElementFinder {
    return this.getPasswordInput()
      .element(by.xpath('..'))
      .element(by.css('.alert'));
  }

  fillEmail(value: string): promise.Promise<void> {
    return this.getEmailInput().sendKeys(value);
  }

  fillPassword(value: string): promise.Promise<void> {
    return this.getPasswordInput().sendKeys(value);
  }

  submit(): promise.Promise<void> {
    return this.getSubmitButton().click();
  }
}
