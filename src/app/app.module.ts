import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/components/login/login.component';
import { WorkflowsComponent } from './workflows/components/workflows/workflows.component';
import { WorkflowComponent } from './workflows/components/workflow/workflow.component';
import { WorkflowBoxComponent } from './workflows/components/workflow-box/workflow-box.component';
import { FieldErrorsComponent } from './field-errors/components/field-errors/field-errors.component';

import { WorkflowsService } from './workflows/services/workflows.service';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  }, {
    path: 'workflows',
    component: WorkflowsComponent
  }, {
    path: 'workflow/:id',
    component: WorkflowComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    WorkflowsComponent,
    WorkflowComponent,
    WorkflowBoxComponent,
    FieldErrorsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes, {enableTracing: true})
  ],
  providers: [
    WorkflowsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
