import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'field-errors',
  templateUrl: './field-errors.component.html',
  styleUrls: ['./field-errors.component.css']
})
export class FieldErrorsComponent implements OnInit {

  @Input() field: any;
  @Input() form: any;

  constructor() {}

  ngOnInit() {
  }
}
