import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        RouterTestingModule
      ]
    }).compileComponents();
  }));

  describe('global structure', () => {
    it('should create the app', async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }));
    it(`should have as title 'Epg'`, async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app.title).toEqual('Epg');
    }));
    it('should render a sidebar', async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('.sidebar')).toBeTruthy();
    }));
    it('should render a router-outlet', async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('router-outlet')).toBeTruthy();
    }));
  });
  
  describe('sidebar', () => {
    let sidebar;

    beforeEach(async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      sidebar = compiled.querySelector('.sidebar');
    }));

    it('should contain a Home item', async(() => {
      expect(sidebar.querySelector('.nav-item:first-child .nav-link').textContent).toEqual('Home');
    }));
    it('should contain a Workflow CRUD item', async(() => {
      expect(sidebar.querySelector('.nav-item:nth-child(2) .nav-link').textContent).toEqual('Workflow CRUD');
    }));
    it('should contain a Logout item', async(() => {
      expect(sidebar.querySelector('.nav-item:last-child .nav-link').textContent).toEqual('Logout');
    }));
  });
});
