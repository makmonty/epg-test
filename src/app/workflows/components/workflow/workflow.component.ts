import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { WorkflowsService } from '../../services/workflows.service';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.css']
})
export class WorkflowComponent implements OnInit {

  workflow: any;

  constructor(
    private workflowsService: WorkflowsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.workflow = id !== 'new' ? this.workflowsService.getById(parseInt(id)) : {
      name: '',
      initBox: {
        name: 'Init',
        type: 'init', // init, end, conditional, action
        next: [
          {
            name: 'End',
            type: 'end',
            next: [],
          }
        ]
      }
    };
  }

  save() {
    this.workflowsService.save(this.workflow);
    this.router.navigate(['/workflows']);
  }
}
