import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'workflow-box',
  templateUrl: './workflow-box.component.html',
  styleUrls: ['./workflow-box.component.css']
})
export class WorkflowBoxComponent implements OnInit {

  @Input() box: any;

  types: any;

  constructor() {}

  ngOnInit() {
    this.types = [
      {
        value: 'action',
        name: 'Action'
      }, {
        value: 'conditional',
        name: 'Conditional'
      }, {
        value: 'end',
        name: 'End'
      },
    ];
  }

  addBox() {
    this.box.next.push({
      name: '',
      type: 'action',
      next: []
    });
  }

  deleteChild(index) {
    this.box.next.splice(index, 1);
  }
}
