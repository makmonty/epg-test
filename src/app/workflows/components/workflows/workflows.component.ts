import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { WorkflowsService } from '../../services/workflows.service';

@Component({
  selector: 'app-workflows',
  templateUrl: './workflows.component.html',
  styleUrls: ['./workflows.component.css']
})
export class WorkflowsComponent implements OnInit {

  workflows: any[];

  constructor(
    private workflowsService: WorkflowsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.getWorkflows();
  }

  edit(workflow: any) {
    this.router.navigate(['/workflow', workflow.id]);
  }

  delete(workflow: any) {
    this.workflowsService.delete(workflow);
    this.getWorkflows();
  }

  getWorkflows() {
    this.workflows = this.workflowsService.getAll();
  }
}
