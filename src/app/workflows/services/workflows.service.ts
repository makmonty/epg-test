import { Injectable } from '@angular/core';
import * as extend from 'extend';

@Injectable()
export class WorkflowsService {

  static lastId: number = 0;

  storage: any;
  storageKey: string = 'EPG_WORKFLOWS';

  constructor() {
    this.storage = localStorage;
  }

  getAll() {
    return JSON.parse(this.storage.getItem(this.storageKey) || '[]');
  }

  save(workflow: any) {
    let workflows = this.getAll();

    if (workflow.id) {
      let wf = this._findById(workflows, workflow.id);
      if (wf) {
        extend(wf, workflow);
      }
    } else {
      workflows.push(workflow);
    }
    this._ensureId(workflow);

    this.saveWorkflows(workflows);
  }

  saveWorkflows(workflows: any[]) {
    workflows.forEach((wf) => {
      this._ensureId(wf);
    });
    this.storage.setItem(this.storageKey, JSON.stringify(workflows));
  }

  getById(id: number) {
    let workflows = this.getAll();
    return this._findById(workflows, id);
  }

  delete(workflow: any) {
    let workflows = this.getAll();
    let index = this._findIndexById(workflows, workflow.id);
    if (index >= 0) {
      workflows.splice(index, 1);
    }
    this.saveWorkflows(workflows);
  }

  private _findById(workflows: any[], id: number) {
    return workflows[this._findIndexById(workflows, id)];
  }

  private _findIndexById(workflows: any[], id: number) {
    return workflows.findIndex(wf => wf.id === id);
  }

  private _ensureId(workflow: any) {
    workflow.id = workflow.id || ++WorkflowsService.lastId;
  }
}
