import { WorkflowsService } from './workflows.service';

describe('WorkflowsService', () => {

  let workflowsService: WorkflowsService;

  beforeEach(() => {
    workflowsService = new WorkflowsService();
    WorkflowsService.lastId = 0;
    localStorage.removeItem(workflowsService.storageKey);
  });

  afterEach(() => {

  });

  describe('#getAll', () => {
    it('should return an empty array when storage is empty', () => {
      spyOn(localStorage, 'getItem').and.returnValue('');
      expect(workflowsService.getAll()).toEqual([]);
    });
    it('should return whatever is on the storage as an object', () => {
      spyOn(localStorage, 'getItem').and.returnValue('["something", "cool"]');
      expect(workflowsService.getAll()).toEqual(['something', 'cool']);
    });
    it('should fail when content of storage is not a JSON string', () => {
      spyOn(localStorage, 'getItem').and.returnValue('wrong content');
      expect(workflowsService.getAll).toThrow();
    });
  });

  describe('#getById', () => {
    it('should find an object with the given id in the storage', () => {
      spyOn(localStorage, 'getItem').and.returnValue('[{"id": 1, "name": "right"}, {"id": 2, "name": "wrong"}]');
      expect(workflowsService.getById(1).name).toBe('right');
    });
    it('should return undefined when id is not present on the storage', () => {
      spyOn(localStorage, 'getItem').and.returnValue('[{"id": 1, "name": "right"}]');
      expect(workflowsService.getById(2)).toBeUndefined();
    });
  });

  describe('#save', () => {
    it('should set the id of an object if it didn\'t have one', () => {
      let obj: any = {name: 'new obj'};
      workflowsService.save(obj);
      expect(obj.id).toBe(1);
    });
    it('should add the item to the storage if it was not there before', () => {
      spyOn(localStorage, 'getItem').and.returnValue('[]');
      let saveStub = spyOn(workflowsService, 'saveWorkflows');

      workflowsService.save({name: 'new workflow'});
      expect(saveStub).toHaveBeenCalledWith([{id: 1, name: 'new workflow'}]);
    });
    it('should extend the existing object when the object already existed', () => {
      spyOn(localStorage, 'getItem').and.returnValue('[{"id": 1, "a": "b"}]');
      let saveStub = spyOn(workflowsService, 'saveWorkflows');

      workflowsService.save({id: 1, name: 'new workflow'});
      expect(saveStub).toHaveBeenCalledWith([{id: 1, name: 'new workflow', a: 'b'}]);
    });
  });

  describe('#saveWorkflows', () => {
    it('should substitute the content of the storage with the passed array', () => {
      let saveSpy = spyOn(localStorage, 'setItem');
      let objs = [
        {id: 1, name: 'name'},
        {id: 2, name: 'another name'}
      ]
      workflowsService.saveWorkflows(objs);
      expect(saveSpy).toHaveBeenCalledWith(workflowsService.storageKey, JSON.stringify(objs));
    });
    it('should set the ids of the objects without id', () => {
      let objs: any = [
        {name: 'name'},
        {name: 'another name'}
      ]
      workflowsService.saveWorkflows(objs);
      expect(objs[0].id).toBe(1);
      expect(objs[1].id).toBe(2);
    });
  });

  describe('#delete', () => {
    it('should delete the item of the storage based on the id of the object passed', () => {
      spyOn(localStorage, 'getItem').and.returnValue('[{"id": 1, "a": "b"}, {"id": 2, "c": "d"}]');
      let saveStub = spyOn(workflowsService, 'saveWorkflows');
  
      workflowsService.delete({id: 1});
      expect(saveStub).toHaveBeenCalledWith([{id: 2, c: 'd'}]);
    });
    it('should leave the storage the same if the passed object didn\'t exist', () => {
      spyOn(localStorage, 'getItem').and.returnValue('[{"id": 1, "a": "b"}, {"id": 2, "c": "d"}]');
      let saveStub = spyOn(workflowsService, 'saveWorkflows');
  
      workflowsService.delete({id: 3});
      expect(saveStub).toHaveBeenCalledWith([{id: 1, a: 'b'}, {id: 2, c: 'd'}]);
    });
  });
});